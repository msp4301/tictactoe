/*
 * Config.h
 *
 *  Created on: Dec 31, 2020
 *
 */

#ifndef CONFIG_H_
#define CONFIG_H_


#define WIFI_SSID       "WIFI_SSID_HERE"
#define WIFI_PASSWORD   "WIFI_PASSWORD_HERE"

#define SERVER_IP       "SERVER_IP_HERE"
#define SERVER_PORT     "SERVER_PORT_HERE"


#endif /* CONFIG_H_ */
